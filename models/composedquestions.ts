"use strict";
export const ComposedQuestions = (sequelize: any, DataTypes: any) => {
  const ComposedQuestions = sequelize.define(
    "composedquestions",
    {
      question_id: DataTypes.INTEGER,
      level_id: DataTypes.INTEGER,
      topic_id: DataTypes.INTEGER
    },
    {}
  );
  ComposedQuestions.associate = function(models: any) {
    // associations can be defined here
  };
  return ComposedQuestions;
};
