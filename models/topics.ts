import { Sequelize, DataType, DataTypes, Model } from "sequelize/types";

export const Topics = (sequelize: any, DataTypes: any): any => {
  const Topics = sequelize.define(
    "topics",
    {
      id: DataTypes.INTEGER,
      name: DataTypes.STRING
    },
    {}
  );
  Topics.associate = function(models: Model) {
    // associations can be defined here
  };
  return Topics;
};
