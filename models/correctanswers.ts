"use strict";
export const CorrecAnswers = (sequelize: any, DataTypes: any) => {
  const CorrectAnswers = sequelize.define(
    "correctanswers",
    {
      composed_question_id: DataTypes.INTEGER,
      answer: DataTypes.STRING
    },
    {}
  );
  CorrectAnswers.associate = function(models: any) {
    // associations can be defined here
  };
  return CorrectAnswers;
};
