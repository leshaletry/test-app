export const Question = (sequelize: any, DataTypes: any) => {
  const Questions = sequelize.define(
    "questions",
    {
      id: DataTypes.STRING
    },
    {}
  );
  Questions.associate = function(models: any) {
    // associations can be defined here
  };
  return Question;
};
