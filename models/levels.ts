"use strict";
export const Levels = (sequelize: any, DataTypes: any) => {
  const Levels = sequelize.define(
    "levels",
    {
      value: DataTypes.INTEGER
    },
    {}
  );
  Levels.associate = function(models: any) {
    // associations can be defined here
  };
  return Levels;
};
