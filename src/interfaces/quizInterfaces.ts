import { Option } from "@slack/types";

export interface QuizSettings {
  select_topic: Option;
  select_level: Option;
}

export interface QuestionItem {
  id?: number;
  text: string;
  answer: string;
  level: string;
  topic: string;
}
export interface QuestionInterface {
  id?: number;
  text: string;
  answer: string;
  level: string;
  topic: string;
}

export interface Question {
  id: number;
  text: string;
}
export interface Topic {
  id: number;
  name: string;
}
export interface Level {
  id: number;
  value: number;
}

export interface QuestionWithId {
  question_id: number;
  text: string;
}
export interface LevelWithId {
  level_id: number;
  value: string;
}
export interface TopicWithId {
  topic_id: number;
  name: string;
}

export interface QuestionInformation {
  question: string | null;
  level: string | null;
  topic: string | null;
}
