export function navigationHead(): any {
  return {
    blocks: [
      {
        type: "section",
        text: {
          type: "plain_text",
          text: "In this section you can add new basic quiz items!",
          emoji: true
        }
      },
      {
        type: "divider"
      },
      {
        type: "actions",
        elements: [
          {
            type: "button",
            action_id: "new_question",
            text: {
              type: "plain_text",
              text: "New question",
              emoji: true
            },
            value: "click_me_123"
          },
          {
            type: "button",
            action_id: "new_topic",
            text: {
              type: "plain_text",
              text: "New topic",
              emoji: true
            },
            value: "click_me_123"
          },
          {
            type: "button",
            action_id: "new_level",
            text: {
              type: "plain_text",
              text: "New level",
              emoji: true
            },
            value: "click_me_123"
          }
        ]
      },
      {
        type: "section",
        text: {
          type: "plain_text",
          text: "Hear you can compose questions and add correct answers!",
          emoji: true
        }
      },
      {
        type: "divider"
      },
      {
        type: "actions",
        elements: [
          {
            type: "button",
            action_id: "compose_question",
            text: {
              type: "plain_text",
              text: "Compose question",
              emoji: true
            },
            value: "click_me_123"
          },
          {
            type: "button",
            action_id: "add_answer",
            text: {
              type: "plain_text",
              text: "Add answer",
              emoji: true
            },
            value: "click_me_123"
          }
        ]
      },
      {
        type: "section",
        text: {
          type: "plain_text",
          text: "Check already passed quizes!",
          emoji: true
        }
      },
      {
        type: "divider"
      },
      {
        type: "actions",
        elements: [
          {
            type: "button",
            action_id: "user_answers",
            text: {
              type: "plain_text",
              text: "User answers",
              emoji: true
            },
            value: "click_me_123"
          }
        ]
      }
    ]
  };
}
