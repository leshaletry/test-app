import { View, Option } from "@slack/types";
import {
  Question,
  Topic,
  Level,
  QuestionWithId,
  LevelWithId,
  TopicWithId,
  QuestionInformation
} from "../interfaces/quizInterfaces";

function selectOption(text: string, value: string) {
  return {
    text: {
      type: "plain_text",
      text: `${text}`,
      emoji: true
    },
    value: String(value)
  };
}

function staticSelect(label: string, options: any[]) {
  return {
    type: "input",
    block_id: `${label.toLocaleLowerCase()}`,
    element: {
      type: "static_select",
      placeholder: {
        type: "plain_text",
        text: "Select an item",
        emoji: true
      },
      options: [...options]
    },
    label: {
      type: "plain_text",
      text: `${label}`,
      emoji: true
    }
  };
}

function createSelectOptions(data: any) {
  if (data[0].question_id) {
    return data.map((item: any) => selectOption(item.text, item.question_id));
  }
  if (data[0].level_id) {
    return data.map((item: any) => selectOption(item.value, item.level_id));
  }
  if (data[0].topic_id) {
    return data.map((item: any) => selectOption(item.name, item.topic_id));
  }

  if (data[0].text) {
    return data.map((item: any) => selectOption(item.text, item.id));
  }
  if (data[0].name) {
    return data.map((item: any) => selectOption(item.name, item.id));
  }
  if (data[0].value) {
    return data.map((item: any) => selectOption(item.value, item.id));
  }
}

export function addQuestionView(
  questions: Question[],
  topics: Topic[],
  levels: Level[]
): View {
  const questionsOptions = createSelectOptions(questions);
  const questionSelect = staticSelect("Question", questionsOptions);

  const topicOptions = createSelectOptions(topics);
  const topicSelect = staticSelect("Topic", topicOptions);

  const levelOptions = createSelectOptions(levels);
  const levelSelect = staticSelect("Level", levelOptions);

  return {
    type: "modal",
    callback_id: "compose_submission",
    title: {
      type: "plain_text",
      text: "Add question",
      emoji: true
    },
    submit: {
      type: "plain_text",
      text: "Submit",
      emoji: true
    },
    close: {
      type: "plain_text",
      text: "Cancel",
      emoji: true
    },
    blocks: [questionSelect, topicSelect, levelSelect]
  };
}

export function addView(type: string): any {
  return {
    type: "modal",
    callback_id: `add_${type}`,
    title: {
      type: "plain_text",
      text: `Add ${type}`,
      emoji: true
    },
    submit: {
      type: "plain_text",
      text: "Submit",
      emoji: true
    },
    close: {
      type: "plain_text",
      text: "Cancel",
      emoji: true
    },
    blocks: [
      {
        type: "input",
        element: {
          type: "plain_text_input"
        },
        label: {
          type: "plain_text",
          text: `${type}`,
          emoji: true
        }
      },
      {
        type: "input",
        element: {
          type: "checkboxes",
          options: [
            {
              text: {
                type: "plain_text",
                text: `<--- Press for add next ${type}`,
                emoji: true
              },
              value: "true"
            }
          ]
        },
        label: {
          type: "plain_text",
          text: `Are you going to add few ${type}s?`,
          emoji: true
        }
      }
    ]
  };
}

export function errorView() {
  return {
    type: "modal",
    title: {
      type: "plain_text",
      text: "Error",
      emoji: true
    },
    close: {
      type: "plain_text",
      text: "Cancel",
      emoji: true
    },
    blocks: [
      {
        type: "divider"
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: "This question is already exsist!"
        }
      }
    ]
  };
}

// blink
export function addCorrectAnswerSettings(
  topics: Topic[],
  levels: Level[],
  mData = ""
): View {
  const { option, composedId } = JSON.parse(mData);

  const topicOptions = createSelectOptions(topics);
  const levelOptions = createSelectOptions(levels);

  return {
    type: "modal",
    callback_id: "added_answer_with_settings",
    title: {
      type: "plain_text",
      text: "Add answer",
      emoji: true
    },
    submit: {
      type: "plain_text",
      text: "Submit",
      emoji: true
    },
    close: {
      type: "plain_text",
      text: "Cancel",
      emoji: true
    },
    blocks: [
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text:
            "Choose question and enter answer, use select for choose question."
        }
      },
      {
        type: "divider"
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `Current question: ${option.text.text}`
        }
      },
      {
        type: "context",
        elements: [
          {
            type: "mrkdwn",
            text: "*Topic: *"
          }
        ]
      },
      {
        type: "divider"
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: "SQL"
        },
        accessory: {
          type: "static_select",
          action_id: "settings_topic",
          placeholder: {
            type: "plain_text",
            text: "Select an item",
            emoji: true
          },
          options: [...topicOptions]
        }
      },
      {
        type: "context",
        elements: [
          {
            type: "mrkdwn",
            text: "*Level: *"
          }
        ]
      },
      {
        type: "divider"
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: "Level 1"
        },
        accessory: {
          type: "static_select",
          action_id: "settings_level",
          placeholder: {
            type: "plain_text",
            text: "Select an item",
            emoji: true
          },
          options: [...levelOptions]
        }
      },
      {
        type: "input",
        element: {
          type: "plain_text_input",
          multiline: true
        },
        label: {
          type: "plain_text",
          text: "Correct answer",
          emoji: true
        }
      }
    ]
  };
}

// blink
export function addCorrectAnswer(
  questions: Question[],
  info: QuestionInformation,
  mData = "",
  action = ""
): View {
  const parsedData = mData ? JSON.parse(mData) : "";
  const questionsOptions = createSelectOptions(questions);

  const initialQuestion: Option = parsedData[action];

  return {
    type: "modal",
    callback_id: "added_answer",
    private_metadata: mData,
    title: {
      type: "plain_text",
      text: "Add answer",
      emoji: true
    },
    submit: {
      type: "plain_text",
      text: "Submit",
      emoji: true
    },
    close: {
      type: "plain_text",
      text: "Cancel",
      emoji: true
    },
    blocks: [
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text:
            "Choose question and enter answer, use select for choose question."
        }
      },
      {
        type: "divider"
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: "Choose question"
        },
        accessory: {
          type: "static_select",
          action_id: "select_question_in_add_answer",
          placeholder: {
            type: "plain_text",
            text: "Select an item",
            emoji: true
          },
          initial_option: initialQuestion,
          options: questionsOptions
        }
      },
      {
        type: "context",
        elements: [
          {
            type: "mrkdwn",
            text: "*Question: *"
          }
        ]
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: info.question || "Choose question"
        }
      },
      {
        type: "divider"
      },
      {
        type: "context",
        elements: [
          {
            type: "mrkdwn",
            text: "*Topic: *"
          }
        ]
      },
      {
        type: "section",
        text: {
          type: "plain_text",
          text: info.topic || "Choose question",
          emoji: true
        }
      },
      {
        type: "divider"
      },
      {
        type: "context",
        elements: [
          {
            type: "mrkdwn",
            text: "*Level: *"
          }
        ]
      },
      {
        type: "section",
        text: {
          type: "plain_text",
          text: info.level ? `Level ${info.level}` : "Choose question",
          emoji: true
        }
      },
      {
        type: "divider"
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: "Wrong question? Use settings -> "
        },
        accessory: {
          type: "button",
          action_id: "answer_settings",
          text: {
            type: "plain_text",
            text: "SETTINGS",
            emoji: true
          },
          value: "true"
        }
      },
      {
        type: "input",
        block_id: "right_answer_input",
        element: {
          type: "plain_text_input",
          multiline: true
        },
        label: {
          type: "plain_text",
          text: "Correct answer",
          emoji: true
        }
      }
    ]
  };
}
