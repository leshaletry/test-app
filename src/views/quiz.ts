import {
  QuizSettings,
  QuestionItem,
  QuestionInterface
} from "./../interfaces/quizInterfaces";
import { InputBlock, View, Option } from "@slack/types";
import pool from "../database/database";
import { createOptions } from "../utils/quizUtils";

export async function starterQuizView(metadata: string): Promise<View> {
  const parsedMetadata: QuizSettings = metadata ? JSON.parse(metadata) : "";

  const topics = await pool.query("SELECT * FROM topics;");
  const topicsRow = createOptions(topics.rows);
  const topicsViews: any = [...topicsRow];

  const levels = await pool.query("SELECT * FROM levels;");
  const levelsRow = createOptions(levels.rows);
  const levelsViews: any = [...levelsRow];

  const initialTopic: Option = parsedMetadata.select_topic ||
    topicsRow[0] || {
      text: {
        type: "plain_text",
        text: "Topic 1",
        emoji: true
      },
      value: "1"
    };

  const initialLevel: Option = parsedMetadata.select_level ||
    levelsRow[0] || {
      text: {
        type: "plain_text",
        text: "Level 1",
        emoji: true
      },
      value: "1"
    };

  return {
    type: "modal",
    callback_id: "view_quiz_starter",
    title: {
      type: "plain_text",
      text: "Quiz app",
      emoji: true
    },
    private_metadata: metadata,
    submit: {
      type: "plain_text",
      text: "START",
      emoji: true
    },
    close: {
      type: "plain_text",
      text: "CLOSE",
      emoji: true
    },
    blocks: [
      {
        type: "context",
        elements: [
          {
            type: "mrkdwn",
            text:
              "Quiz starter modal! Choose appropriate topic and level for you."
          }
        ]
      },
      {
        type: "divider"
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: "Select topic."
        },
        accessory: {
          type: "static_select",
          action_id: "select_topic",
          placeholder: {
            type: "plain_text",
            text: "Select an item",
            emoji: true
          },
          initial_option: initialTopic,
          options: topicsViews
        }
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: "Select level."
        },
        accessory: {
          type: "static_select",
          action_id: "select_level",
          placeholder: {
            type: "plain_text",
            text: "Select an item",
            emoji: true
          },
          initial_option: initialLevel,
          options: levelsViews
        }
      }
    ]
  };
}

function inputTemplate(text: string, questionId: number): InputBlock {
  return {
    type: "input",
    block_id: `input_question_${questionId}`,
    element: {
      type: "plain_text_input"
    },
    label: {
      type: "plain_text",
      text: text,
      emoji: true
    }
  };
}

async function generateQuestions(
  questionsId: number[]
): Promise<QuestionInterface[]> {
  const questions = await pool.query(
    `SELECT * FROM questions WHERE id = ANY($1::int[])`,
    [questionsId]
  );
  return questions.rows;
}

async function questions(level = "1", topic = "1"): Promise<InputBlock[]> {
  const questions = await pool.query(
    `SELECT question_id FROM composedqustion 
    WHERE level_id = ${Number(level)} AND topic_id = ${Number(topic)}`
  );

  const questionsId: number[] = questions.rows.map(
    (item: any) => item.question_id
  );

  const appropriateQuestions: QuestionItem[] = await generateQuestions(
    questionsId
  );
  return appropriateQuestions.map((item: any) =>
    inputTemplate(item.text, item.id)
  );
}

export async function blockQuestions(data: string): Promise<View> {
  const { select_level, select_topic }: QuizSettings = JSON.parse(data);

  const questionsArray: InputBlock[] = await questions(
    select_level.value,
    select_topic.value
  );

  return {
    type: "modal",
    callback_id: "view_questions",
    title: {
      type: "plain_text",
      text: "Questions"
    },
    blocks: [
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: "Lorem "
        },
        block_id: "text1"
      },
      ...questionsArray
    ],
    submit: {
      type: "plain_text",
      text: "Submit"
    },
    private_metadata: ""
  };
}

export function quizResult(result: string): View {
  return {
    type: "modal",
    callback_id: "view_questions",
    title: {
      type: "plain_text",
      text: "Questions"
    },
    blocks: [
      {
        type: "section",
        text: {
          type: "plain_text",
          text: result,
          emoji: true
        }
      }
    ]
  };
}
