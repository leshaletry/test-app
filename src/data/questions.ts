import { QuestionInterface } from "../interfaces/quizInterfaces";

const questionsData: QuestionInterface[] = [
  {
    text: "question topic 1 text 1 level 1",
    answer: "1",
    level: "1",
    topic: "1"
  },
  {
    text: "question topic 1 text 2 level 1",
    answer: "2",
    level: "1",
    topic: "1"
  },
  {
    text: "question topic 1 text 1 level 2",
    answer: "1",
    level: "2",
    topic: "1"
  },
  {
    text: "question topic 1 text 2 level 2",
    answer: "2",
    level: "2",
    topic: "1"
  },

  {
    text: "question topic 2 text 1 level 1",
    answer: "1",
    level: "1",
    topic: "2"
  },
  {
    text: "question topic 2 text 2 level 1",
    answer: "2",
    level: "1",
    topic: "2"
  },
  {
    text: "question topic 2 text 1 level 2",
    answer: "1",
    level: "2",
    topic: "2"
  },
  {
    text: "question topic 2 text 2 level 2",
    answer: "2",
    level: "2",
    topic: "2"
  },

  {
    text: "question topic 3 text 1 level 1",
    answer: "1",
    level: "1",
    topic: "3"
  },
  {
    text: "question topic 3 text 2 level 1",
    answer: "2",
    level: "1",
    topic: "3"
  },
  {
    text: "question topic 3 text 1 level 2",
    answer: "1",
    level: "2",
    topic: "3"
  },
  {
    text: "question topic 3 text 2 level 2",
    answer: "2",
    level: "2",
    topic: "3"
  }
];

export default questionsData;
