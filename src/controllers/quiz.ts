import { getValue, getQuizResult } from "./../utils/quizUtils";
import * as quizViews from "../views/quiz";
import {
  CommandMiddlewareArgs,
  ViewMiddlewareArgs,
  ActionMiddlewareArgs
} from "../types/types";
import { BlockAction, StaticSelectAction, ViewOutput } from "@slack/bolt";
const { WebClient } = require("@slack/web-api");

type Args = ActionMiddlewareArgs<BlockAction<StaticSelectAction>>;

// test
export async function quizSlashHandler({
  ack,
  payload,
  context
}: CommandMiddlewareArgs): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = payload;
  const view = await quizViews.starterQuizView("");
  await client.views.open({ trigger_id, view } as any);
}

// dev -> test
export async function quizStart({
  ack,
  view
}: ViewMiddlewareArgs): Promise<void> {
  const data = view.private_metadata;
  ack({
    response_action: "push",
    view: await quizViews.blockQuestions(data)
  } as any);
}

// dev -> test
export async function quizFinish({
  ack,
  view,
  body,
  context
}: ViewMiddlewareArgs): Promise<void> {
  const client = new WebClient(context.botToken);
  // @ts-ignore
  const answers = view.state.values;
  const { private_metadata } = view;
  const rightAnswers = Object.values(private_metadata.split("-"));
  const tmpAnswers = Object.entries(answers);
  const answersNormal = tmpAnswers.reduce((acc: any, item: any) => {
    acc[item[0]] = getValue(item[1]);
    return acc;
  }, {});
  const arrayOfNormalAnswers: string[] = Object.values(answersNormal);
  const result = getQuizResult(arrayOfNormalAnswers, rightAnswers);

  ack({ response_action: "clear" } as any);
  // @ts-ignore
  const { trigger_id } = body;
  const views = quizViews.quizResult(result);
  await client.views.open({ trigger_id, view: views } as any);
}

export async function selectionUpdate({ ack, body, action, context }: Args) {
  ack();
  const client = new WebClient(context.botToken);
  const { action_id: actionId } = action;
  const selectedItem = action.selected_option;
  // @ts-ignore
  const { id, private_metadata: privateMetadata } = body.view as ViewOutput;
  const parsedPrivateMetadata = privateMetadata
    ? JSON.parse(privateMetadata)
    : "";
  const metadata = {
    ...parsedPrivateMetadata,
    [actionId]: selectedItem
  };
  const metadataString = JSON.stringify(metadata);
  const view = await quizViews.starterQuizView(metadataString);
  await client.views.update({ view_id: id, view } as any);
}
