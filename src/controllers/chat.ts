import {
  MessageMiddlewareArgs,
  ActionMiddlewareArgs,
  ViewMiddlewareArgs
} from "../types/types";
import { navigationHead } from "../views/chatViews";
const { WebClient } = require("@slack/web-api");
import { View } from "@slack/types";
import * as questionsViews from "../views/addQuestion";
import {
  BlockAction,
  ButtonAction,
  ViewOutput,
  StaticSelectAction
} from "@slack/bolt";
import {
  Question,
  Level,
  Topic,
  QuestionWithId,
  LevelWithId,
  TopicWithId,
  QuestionInformation
} from "../interfaces/quizInterfaces";
import pool from "../database/database";
import { getValue } from "../utils/quizUtils";

type Args = ActionMiddlewareArgs<BlockAction<ButtonAction>>;
type ArgsSelect = ActionMiddlewareArgs<BlockAction<StaticSelectAction>>;

// test
export async function sendHeadNavigation({
  message,
  say
}: MessageMiddlewareArgs): Promise<void> {
  say(`Hey there <@${message.user}>!`);
  say(navigationHead());
}

export async function openNewQuestionModal({
  ack,
  body,
  context
}: Args): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = body;
  const view: View = questionsViews.addView("question");
  await client.views.open({ trigger_id, view } as any);
}

export async function openNewLevelModal({
  body,
  ack,
  context
}: Args): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = body;
  const view: View = questionsViews.addView("level");
  await client.views.open({ trigger_id, view } as any);
}

export async function openNewTopicModal({
  body,
  ack,
  context
}: Args): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = body;
  const view: View = questionsViews.addView("topic");
  await client.views.open({ trigger_id, view } as any);
}

export async function openComposeQuestionModal({
  body,
  ack,
  context
}: Args): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = body;

  const [questions, levels, topics] = await Promise.all([
    pool.query<Question>(`SELECT id, text FROM questions;`),
    pool.query<Level>(`SELECT id, value FROM levels;`),
    pool.query<Topic>(`SELECT id, name FROM topics;`)
  ]);

  const view: View = questionsViews.addQuestionView(
    questions.rows,
    topics.rows,
    levels.rows
  );
  await client.views.open({ trigger_id, view } as any);
}

export async function openAddAnswerModal({
  body,
  ack,
  context
}: Args): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = body;

  const [questions] = await Promise.all([
    pool.query<Question>(`SELECT id, text FROM questions;`)
  ]);

  const info: QuestionInformation = {
    question: null,
    level: null,
    topic: null
  };
  const view: View = questionsViews.addCorrectAnswer(questions.rows, info);

  await client.views.open({ trigger_id, view } as any);
}

// test
export async function openUserAnswersSettingsModal({
  body,
  ack,
  context
}: Args): Promise<void> {
  ack();
  // @ts-ignore
  const { private_metadata: privateMetadata } = body.view;

  const { composedId, select_question_in_add_answer: option } = JSON.parse(
    privateMetadata
  );

  const [topics, levels] = await Promise.all([
    pool.query<Topic>(`
      SELECT
        topic_id, name
      FROM composedquestions
      JOIN questions ON question_id = questions.id
      JOIN topics ON topic_id = topics.id
      WHERE
      question_id = ${composedId}
    `),
    pool.query<Level>(`
      SELECT
        level_id, value
      FROM composedquestions
      JOIN questions ON composedquestions.question_id = questions.id
      JOIN levels ON composedquestions.topic_id = levels.id
      WHERE
      question_id = ${composedId}
    `)
  ]);

  const meta = {
    composedId,
    option
  };

  const client = new WebClient(context.botToken);
  const view: View = questionsViews.addCorrectAnswerSettings(
    topics.rows,
    levels.rows,
    JSON.stringify(meta)
  );

  // @ts-ignore
  const { id } = body.view as ViewOutput;
  await client.views.update({ view_id: id, view } as any);
}

// dev -> test
export async function openUserAnswersModal({
  body,
  ack,
  say
}: any): Promise<void> {
  ack();
  say(`User answers!`);
}

// test
export async function selectQustionInAddAnswer({
  ack,
  body,
  action,
  context
}: ArgsSelect): Promise<void> {
  ack();

  const client = new WebClient(context.botToken);
  const { selected_option: selectedItem, action_id: actionId } = action;

  const [questions] = await Promise.all([
    pool.query<Question>(`SELECT id, text FROM questions;`)
  ]);
  const { rows: indexes } = await pool.query<any>(
    `SELECT * FROM composedquestions WHERE question_id = ${selectedItem.value} LIMIT 1;`
  );

  const { rows: questionInfoId } = await pool.query<any>(
    `
      SELECT
      composedquestions.id AS "compose_id",
        question_id, text,
        level_id, value,
        topic_id, name
      FROM composedquestions
      JOIN questions ON question_id = questions.id
      JOIN levels ON level_id = levels.id
      JOIN topics ON topic_id = topics.id
      WHERE
      questions.id = ${indexes[0].question_id}
      AND levels.id = ${indexes[0].level_id}
      AND topics.id = ${indexes[0].topic_id}
    `
  );

  // @ts-ignore
  const { id, private_metadata: privateMetadata } = body.view as ViewOutput;
  const parsedPrivateMetadata = privateMetadata
    ? JSON.parse(privateMetadata)
    : "";
  const metadata = {
    ...parsedPrivateMetadata,
    composedId: questionInfoId[0].compose_id,
    [actionId]: selectedItem
  };
  const metadataString = JSON.stringify(metadata);
  const info: QuestionInformation = {
    question: questionInfoId[0].text || null,
    level: questionInfoId[0].value || null,
    topic: questionInfoId[0].name || null
  };
  const view: View = questionsViews.addCorrectAnswer(
    questions.rows,
    info,
    metadataString,
    actionId
  );

  await client.views.update({ view_id: id, view } as any);
}

// test priory
export async function addCorrectAnswerSubmission({
  ack,
  view,
  body,
  context
}: ViewMiddlewareArgs): Promise<void> {
  ack();
  // @ts-ignore
  const { values } = view.state;
  const answer = getValue(values.right_answer_input);
  const { composedId } = JSON.parse(view.private_metadata);

  const rowCount = await pool.query(
    `SELECT COUNT (*) FROM correctanswers WHERE composed_question_id = ${Number(
      composedId
    )};`
  );
  const count = Number(rowCount.rows[0].count);
  if (count === 0) {
    await pool.query(
      `INSERT INTO correctanswers(composed_question_id, answer) VALUES($1, $2)`,
      [composedId, answer]
    );
    return;
  }
  ack({ response_action: "clear" } as any);
  const client = new WebClient(context.botToken);
  //@ts-ignore
  const { trigger_id } = body;
  const views = questionsViews.errorView();
  await client.views.open({ trigger_id, view: views });
}

// dev -> test
export async function addCorrectAnswerSettingsSubmission({
  ack,
  view,
  body,
  context
}: ViewMiddlewareArgs): Promise<void> {
  ack();
  // @ts-ignore
  const { values } = view.state;
}

// check WTF
export async function selectQuestionSettings({
  ack,
  body,
  action,
  context
}: ArgsSelect): Promise<void> {
  ack();
  // @ts-ignore
  const { selected_option: selectedItem, action_id: actionId } = action;
  const client = new WebClient(context.botToken);

  // @ts-ignore
  const { id, private_metadata: privateMetadata } = body.view as ViewOutput;
  const parsedPrivateMetadata = privateMetadata
    ? JSON.parse(privateMetadata)
    : "";
  const metadata = {
    ...parsedPrivateMetadata,
    [actionId]: selectedItem
  };

  const metadataString = JSON.stringify(metadata);
  // const view: View = questionsViews.addCorrectAnswerSettings(
  //   questions.rows,
  //   topics.rows,
  //   levels.rows,
  //   metadataString
  // );
  // await client.views.update({ view_id: id, view } as any);
}
