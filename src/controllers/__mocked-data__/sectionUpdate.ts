export const mockStringifiedView =
  '{"type":"modal","callback_id":"view_quiz_starter","title":{"type":"plain_text","text":"Quiz app","emoji":true},"private_metadata":"{\\"select_topic\\":{\\"text\\":{\\"type\\":\\"plain_text\\",\\"text\\":\\"JavaScript\\",\\"emoji\\":true},\\"value\\":\\"1\\"},\\"select_level\\":{\\"text\\":{\\"type\\":\\"plain_text\\",\\"text\\":\\"1\\",\\"emoji\\":true},\\"value\\":\\"1\\"}}","submit":{"type":"plain_text","text":"START","emoji":true},"close":{"type":"plain_text","text":"CLOSE","emoji":true},"blocks":[{"type":"context","elements":[{"type":"mrkdwn","text":"Quiz starter modal! Choose appropriate topic and level for you."}]},{"type":"divider"},{"type":"section","text":{"type":"mrkdwn","text":"Select topic."},"accessory":{"type":"static_select","action_id":"select_topic","placeholder":{"type":"plain_text","text":"Select an item","emoji":true},"initial_option":{"text":{"type":"plain_text","text":"JavaScript","emoji":true},"value":"1"},"options":[{"text":{"type":"plain_text","text":"JavaScript","emoji":true},"value":"1"},{"text":{"type":"plain_text","text":"SQL","emoji":true},"value":"2"},{"text":{"type":"plain_text","text":"PostgreSQL","emoji":true},"value":"3"},{"text":{"type":"plain_text","text":"PostgreSQL1","emoji":true},"value":"4"}]}},{"type":"section","text":{"type":"mrkdwn","text":"Select level."},"accessory":{"type":"static_select","action_id":"select_level","placeholder":{"type":"plain_text","text":"Select an item","emoji":true},"initial_option":{"text":{"type":"plain_text","text":"1","emoji":true},"value":"1"},"options":[{"text":{"type":"plain_text","text":"1","emoji":true},"value":"1"}]}}]}';

export const privateMeatdata = {
  select_topic: {
    text: { type: "plain_text", text: "JavaScript", emoji: true },
    value: "1"
  },
  select_level: {
    text: { type: "plain_text", text: "1", emoji: true },
    value: "1"
  }
};
