export const privateMetadata = {
  composedId: 1,
  select_question_in_add_answer: {
    text: { type: "plain_text", text: "How to declare variable?", emoji: true },
    value: "1"
  }
};

export const privateMetadataAnswers = JSON.stringify(privateMetadata);
