export const questions = [{ id: 1, text: "How to declare variable?" }];
export const topics = [
  { id: 1, name: "JavaScript" },
  { id: 2, name: "SQL" },
  { id: 3, name: "PostgreSQL" },
  { id: 4, name: "PostgreSQL1" }
];
export const levels = [{ id: 1, value: 1 }];

export const compose = [{ id: 1, question_id: 1, level_id: 1, topic_id: 1 }];

export const questionInfoId = [
  {
    compose_id: 1,
    question_id: 1,
    text: "How to declare variable?",
    level_id: 1,
    value: 1,
    topic_id: 1,
    name: "JavaScript"
  }
];

const metaTemp = {
  composedId: 1,
  select_question_in_add_answer: {
    text: { type: "plain_text", text: "How to declare variable?", emoji: true },
    value: "1"
  }
};

export const metadataString = JSON.stringify(metaTemp);
