import { CommandMiddlewareArgs, ViewMiddlewareArgs } from "../types/types";
import * as questionsViews from "../views/addQuestion";
import { View } from "@slack/types";
import pool from "../database/database";
import { getStateData } from "../utils/quizUtils";
import { Question, Topic, Level } from "../interfaces/quizInterfaces";
const { WebClient } = require("@slack/web-api");

export async function addHandlerLevel({
  ack,
  payload,
  context
}: CommandMiddlewareArgs): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = payload;
  const view: View = questionsViews.addView("level");
  await client.views.open({ trigger_id, view } as any);
}

export async function addHandlerTopic({
  ack,
  payload,
  context
}: CommandMiddlewareArgs): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = payload;
  const view: View = questionsViews.addView("topic");
  await client.views.open({ trigger_id, view } as any);
}

export async function addHandlerQuestion({
  ack,
  payload,
  context
}: CommandMiddlewareArgs): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = payload;
  const view: View = questionsViews.addView("question");
  await client.views.open({ trigger_id, view } as any);
}

export async function addHandlerCorrectAnswer({
  ack,
  payload,
  context
}: CommandMiddlewareArgs): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = payload;
  const view: View = questionsViews.addView("answer");
  await client.views.open({ trigger_id, view } as any);
}

export async function addSubmissionLevel({
  ack,
  view
}: ViewMiddlewareArgs): Promise<void> {
  ack();
  // @ts-ignore
  const { values } = view.state;
  const value = Object.values(getStateData(values))[0];
  await pool.query(`INSERT INTO levels(value) VALUES($1)`, [value]);
}

export async function addSubmissionTopic({
  ack,
  view
}: ViewMiddlewareArgs): Promise<void> {
  ack();
  // @ts-ignore
  const { values } = view.state;
  const value = Object.values(getStateData(values))[0];
  await pool.query(`INSERT INTO topics(name) VALUES($1)`, [value]);
}

export async function addSubmissionQuestion({
  ack,
  view
}: ViewMiddlewareArgs): Promise<void> {
  ack();
  // @ts-ignore
  const { values } = view.state;
  const value = Object.values(getStateData(values))[0];
  const res = await pool.query(`INSERT INTO questions(text) VALUES($1)`, [
    value
  ]);
}

export async function composeHandler({
  ack,
  payload,
  context
}: CommandMiddlewareArgs): Promise<void> {
  ack();
  const client = new WebClient(context.botToken);
  const { trigger_id } = payload;

  const [questions, levels, topics] = await Promise.all([
    pool.query<Question>(`SELECT id, text FROM questions;`),
    pool.query<Level>(`SELECT id, value FROM levels;`),
    pool.query<Topic>(`SELECT id, name FROM topics;`)
  ]);

  const view: View = questionsViews.addQuestionView(
    questions.rows,
    topics.rows,
    levels.rows
  );
  await client.views.open({ trigger_id, view } as any);
}

export async function composeViewSubmission({
  ack,
  view,
  context,
  body
}: ViewMiddlewareArgs): Promise<void> {
  ack();
  // @ts-ignore
  const { values } = view.state;
  const { question, topic, level } = getStateData(values);

  // id тока
  const rowCount = await pool.query(
    `SELECT COUNT( * )
    FROM composedquestions WHERE
    question_id = ${Number(question)} AND level_id = ${Number(
      level
    )} AND topic_id = ${Number(topic)};`
  );
  const count = Number(rowCount.rows[0].count);
  if (count === 0) {
    await pool.query(
      `INSERT INTO composedquestions(question_id, level_id, topic_id) VALUES($1, $2, $3)`,
      [Number(question), Number(level), Number(topic)]
    );
    return;
  }

  ack({ response_action: "clear" } as any);
  const client = new WebClient(context.botToken);
  //@ts-ignore
  const { trigger_id } = body;
  const views = questionsViews.errorView();
  await client.views.open({ trigger_id, view: views });
  return;
}
