import { View } from "@slack/types";
import { questions, levels, topics } from "./../__mocked-data__/rows";
import { openComposeQuestionModal } from "../chat";
import pool from "../../database/database";
import { addQuestionView } from "../../views/addQuestion";

jest.mock("../../database/database", () => {
  return {
    query: jest
      .fn()
      .mockResolvedValueOnce({ rows: questions })
      .mockResolvedValueOnce({ rows: levels })
      .mockResolvedValueOnce({ rows: topics })
  };
});

const open = jest.fn().mockResolvedValue(true);

jest.mock("@slack/web-api", () => ({
  WebClient: class {
    public views = {
      open: open
    };
  }
}));

describe("Open compose quetion modal", () => {
  const ack = jest.fn();
  const body = { trigger_id: "trigger_id" };
  const context = { botToken: "botToken" };
  const args = { body, ack, context } as any;

  beforeEach(() => jest.clearAllMocks());

  it("Should open compose modal with right DB query and and invoke open method with right parameters", async () => {
    const trigger_id = "trigger_id";
    const view: View = addQuestionView(questions, topics, levels);

    await openComposeQuestionModal(args);

    expect(pool.query).toHaveBeenCalledWith(`SELECT id, text FROM questions;`);
    expect(pool.query).toHaveBeenCalledWith(`SELECT id, value FROM levels;`);
    expect(pool.query).toHaveBeenCalledWith(`SELECT id, name FROM topics;`);
    expect(open).toHaveBeenCalledWith({ trigger_id, view } as any);
  });
});
