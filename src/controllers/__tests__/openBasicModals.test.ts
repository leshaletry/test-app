import { View } from "@slack/types";
import {
  openNewQuestionModal,
  openNewLevelModal,
  openNewTopicModal
} from "../chat";
import { addView } from "../../views/addQuestion";

const open = jest.fn().mockResolvedValue(true);

jest.mock("@slack/web-api", () => ({
  WebClient: class {
    public views = {
      open: open
    };
  }
}));

const ack = jest.fn();
const body = { trigger_id: "trigger_id" };
const context = { botToken: "botToken" };
const args = { ack, body, context } as any;

describe("Open basic modals", () => {
  beforeEach(() => jest.clearAllMocks());

  it("Should open question modal", async () => {
    const trigger_id = "trigger_id";
    const view: View = addView("question");

    await openNewQuestionModal(args);

    expect(open).toHaveBeenCalledWith({ trigger_id, view } as any);
  });

  it("Should open topic modal", async () => {
    const trigger_id = "trigger_id";
    const view: View = addView("topic");

    await openNewTopicModal(args);

    expect(open).toHaveBeenCalledWith({ trigger_id, view } as any);
  });

  it("Should open level modal", async () => {
    const trigger_id = "trigger_id";
    const view: View = addView("level");

    await openNewLevelModal(args);

    expect(open).toHaveBeenCalledWith({ trigger_id, view } as any);
  });
});
