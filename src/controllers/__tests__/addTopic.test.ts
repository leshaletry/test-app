import { addSubmissionTopic } from "../addQuestion";
import pool from "../../database/database";

jest.mock("../../database/database", () => ({
  query: jest.fn().mockResolvedValue(true)
}));

describe("Add topic controller", () => {
  const ack = jest.fn();
  const view = {
    state: {
      values: {
        OPzej: { X7Q: { type: "plain_text_input", value: "PostgreSQL" } }
      }
    }
  };
  const args = { ack, view } as any;

  beforeEach(() => jest.clearAllMocks());

  it("Should add new topic", async () => {
    await addSubmissionTopic(args);

    expect(ack).toHaveBeenCalled();
  });

  it("Should save new topic to DB", async () => {
    await addSubmissionTopic(args);

    expect(
      pool.query
    ).toHaveBeenCalledWith(`INSERT INTO topics(name) VALUES($1)`, [
      "PostgreSQL"
    ]);
  });
});
