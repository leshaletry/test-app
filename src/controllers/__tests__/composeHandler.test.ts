import { questions, levels, topics } from "./../__mocked-data__/rows";
import { composeHandler } from "../addQuestion";
import * as questionsViews from "../../views/addQuestion";

jest.mock("../../database/database", () => {
  return {
    query: jest
      .fn()
      .mockResolvedValueOnce({ rows: questions })
      .mockResolvedValueOnce({ rows: levels })
      .mockResolvedValueOnce({ rows: topics })
  };
});

const open = jest.fn().mockResolvedValue(true);

jest.mock("@slack/web-api", () => ({
  WebClient: class {
    public views = {
      open: open
    };
  }
}));

describe("Compose new question", () => {
  const ack = jest.fn();
  const context = { botToken: "token" };
  const payload = { trigger_id: "124254245" };

  beforeEach(() => jest.clearAllMocks());

  it("Should open modal for add new compose question", async () => {
    const view = questionsViews.addQuestionView(questions, topics, levels);
    const { trigger_id } = payload;

    await composeHandler({ ack, context, payload } as any);

    expect(open).toHaveBeenCalledWith({ trigger_id, view } as any);
  });
});
