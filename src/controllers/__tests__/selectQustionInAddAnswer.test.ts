import { View } from "@slack/types";
import { QuestionInformation } from "./../../interfaces/quizInterfaces";
import {
  questions,
  topics,
  compose,
  questionInfoId,
  metadataString
} from "./../__mocked-data__/rows";
import { selectQustionInAddAnswer } from "../chat";
import pool from "../../database/database";
import { addCorrectAnswer } from "../../views/addQuestion";

jest.mock("../../database/database", () => {
  return {
    query: jest
      .fn()
      .mockResolvedValueOnce({ rows: questions })
      .mockResolvedValueOnce({ rows: compose })
      .mockResolvedValueOnce({ rows: questionInfoId })
  };
});

const update = jest.fn().mockResolvedValue(true);

jest.mock("@slack/web-api", () => ({
  WebClient: class {
    public views = {
      update: update
    };
  }
}));

const privateMeta = JSON.stringify({
  composedId: 1,
  select_question_in_add_answer: {
    text: { type: "plain_text", text: "How to declare variable?", emoji: true },
    value: "1"
  }
});

describe("Select option in anser setting", () => {
  const ack = jest.fn();
  const body = { view: { id: "VVDKB615H", private_metadata: privateMeta } };
  const context = { botToken: "token" };
  const action = {
    selected_option: {
      text: {
        type: "plain_text",
        text: "How to declare variable?",
        emoji: true
      },
      value: "1"
    },
    action_id: "select_question_in_add_answer"
  };

  it("Should select question in add answer modal", async () => {
    await selectQustionInAddAnswer({ ack, body, action, context } as any);
    expect(pool.query).toHaveBeenCalledWith(`SELECT id, text FROM questions;`);
    expect(pool.query).toHaveBeenCalledWith(
      `SELECT * FROM composedquestions WHERE question_id = 1 LIMIT 1;`
    );
    expect(pool.query).toHaveBeenCalledWith(`
      SELECT
      composedquestions.id AS "compose_id",
        question_id, text,
        level_id, value,
        topic_id, name
      FROM composedquestions
      JOIN questions ON question_id = questions.id
      JOIN levels ON level_id = levels.id
      JOIN topics ON topic_id = topics.id
      WHERE
      questions.id = ${compose[0].question_id}
      AND levels.id = ${compose[0].level_id}
      AND topics.id = ${compose[0].topic_id}
    `);
  });

  it("Should invoke update method in select question in add answer model with right args", () => {
    const info: QuestionInformation = {
      question: questionInfoId[0].text,
      level: String(questionInfoId[0].value),
      topic: questionInfoId[0].name
    };
    const actionId = "select_question_in_add_answer";
    const view: View = addCorrectAnswer(
      questions,
      info,
      metadataString,
      actionId
    );

    expect(update).toHaveBeenCalledWith({ view_id: "VVDKB615H", view } as any);
  });
});
