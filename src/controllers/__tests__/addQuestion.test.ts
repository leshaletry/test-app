import { addSubmissionQuestion } from "../addQuestion";
import pool from "../../database/database";

jest.mock("../../database/database", () => ({
  query: jest.fn().mockResolvedValue(true)
}));

describe("Add question controller", () => {
  const ack = jest.fn();
  const view = {
    state: {
      values: {
        OPzej: { X7Q: { type: "plain_text_input", value: "What is SELECT?" } }
      }
    }
  };
  const args = { ack, view } as any;

  beforeEach(() => jest.clearAllMocks());

  it("Shold add new question", async () => {
    await addSubmissionQuestion(args);

    expect(ack).toHaveBeenCalled();
  });

  it("Should save new question to DB", async () => {
    await addSubmissionQuestion(args);

    expect(
      pool.query
    ).toHaveBeenCalledWith(`INSERT INTO questions(text) VALUES($1)`, [
      "What is SELECT?"
    ]);
  });
});
