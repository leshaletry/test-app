import { addSubmissionLevel } from "../addQuestion";
import pool from "../../database/database";

jest.mock("../../database/database", () => ({
  query: jest.fn().mockResolvedValue(true)
}));

describe("Add levels controller", () => {
  const ack = jest.fn();
  const view = {
    state: {
      values: {
        OPzej: { X7Q: { type: "plain_text_input", value: "1" } }
      }
    }
  };
  const args = { ack, view } as any;

  beforeEach(() => jest.clearAllMocks());

  it("Should add new level", async () => {
    await addSubmissionLevel(args);

    expect(ack).toHaveBeenCalled();
  });

  it("Should save new level to DB", async () => {
    await addSubmissionLevel(args);

    expect(
      pool.query
    ).toHaveBeenCalledWith(`INSERT INTO levels(value) VALUES($1)`, [
      "1"
    ]);
  });
});
