import { mockStringifiedView, privateMeatdata } from './../__mocked-data__/sectionUpdate';
import { selectionUpdate } from "../quiz";
import { starterQuizView } from "../../views/quiz";

const update = jest.fn().mockResolvedValue(true);

jest.mock("@slack/web-api", () => ({
  WebClient: class {
    public views = {
      update: update
    };
  }
}));

describe("Selection update", () => {
  beforeEach(() => jest.clearAllMocks());

  it("Should update selection", async () => {
    const stringPrivatemetadata = JSON.stringify(privateMeatdata);
    const mockedView = JSON.parse(mockStringifiedView);

    const view = await starterQuizView(stringPrivatemetadata);

    expect(view).toEqual(mockedView);
  });

  it("Should invoke method update with right parameters (view_id, view)", async () => {
    const view_id = "VVCLSPFC1";
    const stringPrivatemetadata = JSON.stringify(privateMeatdata);
    const view = await starterQuizView(stringPrivatemetadata);
    const ack = jest.fn();
    const body = {
      view: { id: view_id, private_metadata: stringPrivatemetadata }
    };
    const action = { action_id: "action_id" };
    const context = { botToken: "botToken" };
    const args = { ack, body, action, context } as any;

    await selectionUpdate(args);

    expect(update).toHaveBeenCalledWith({ view_id, view } as any);
  });
});
