import { app } from "./app";
import * as quizControllers from "./controllers/quiz";
import * as addControllers from "./controllers/addQuestion";
import { decorated } from "./decorators/Catch";
import pool from "./database/database";
import * as chat from "./controllers/chat";
import sequelize from "./database/postgres";

app.command("/quiz", decorated(quizControllers.quizSlashHandler));

app.command("/add-question", decorated(addControllers.addHandlerQuestion));

app.command("/add-topic", decorated(addControllers.addHandlerTopic));

app.command("/add-level", decorated(addControllers.addHandlerLevel));

app.command("/compose-question", decorated(addControllers.composeHandler));

app.action("select_topic", decorated(quizControllers.selectionUpdate));

app.action("select_level", decorated(quizControllers.selectionUpdate));

app.view("view_quiz_starter", decorated(quizControllers.quizStart));

app.view("view_questions", decorated(quizControllers.quizFinish));

app.view("add_topic", decorated(addControllers.addSubmissionTopic));

app.view("add_level", decorated(addControllers.addSubmissionLevel));

app.view("add_question", decorated(addControllers.addSubmissionQuestion));

app.view("compose_submission", decorated(addControllers.composeViewSubmission));

// chat
app.message("chat", decorated(chat.sendHeadNavigation));

app.action("new_question", decorated(chat.openNewQuestionModal));

app.action("new_topic", decorated(chat.openNewTopicModal));

app.action("new_level", decorated(chat.openNewLevelModal));

app.action("compose_question", decorated(chat.openComposeQuestionModal));

app.action("add_answer", decorated(chat.openAddAnswerModal));

app.action("user_answers", decorated(chat.openUserAnswersModal));

app.action("answer_settings", decorated(chat.openUserAnswersSettingsModal));
app.action("settings_level", decorated(chat.selectQuestionSettings));
app.action("settings_question", decorated(chat.selectQuestionSettings));
app.action("settings_topic", decorated(chat.selectQuestionSettings));

app.action(
  "select_question_in_add_answer",
  decorated(chat.selectQustionInAddAnswer)
);

app.view("added_answer", decorated(chat.addCorrectAnswerSubmission));
app.view(
  "added_answer_with_settings",
  decorated(chat.addCorrectAnswerSettingsSubmission)
);

(async () => {
  await app.start(8080);
  console.log("⚡️ Bolt app is running!");
  sequelize
    .sync({})
    .then(() => console.log("⚡️ DB Connected!"))
    .catch((error: any) => {
      console.log(error);
    });
})();
