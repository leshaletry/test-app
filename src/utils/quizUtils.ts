export function getValue(obj: any) {
  let value: any = null;
  for (let key in obj) {
    if (obj[key].type === "plain_text_input") {
      value = obj[key].value;
    } else if (obj[key].type === "checkboxes") {
      value = obj[key].selected_options;
    } else if (obj[key].type === "static_select") {
      value = obj[key].selected_option.value;
    } else {
      value = null;
    }
  }
  return value;
}

export function getQuizResult(answers: string[], rightAnswers: string[]) {
  let countUserRightAnswer = 0;
  answers.forEach((item: string, index: number) => {
    item === rightAnswers[index] ? countUserRightAnswer++ : null;
  });
  return `result: ${countUserRightAnswer}/${rightAnswers.length}`;
}

export function getStateData(data: any) {
  const tmpData = Object.entries(data);
  const normalData = tmpData.reduce((acc: any, item: any) => {
    acc[item[0]] = getValue(item[1]);
    return acc;
  }, {});
  return normalData;
}

export function createOption(text: string, value: string) {
  return {
    text: {
      type: "plain_text",
      text: text,
      emoji: true
    },
    value: value
  };
}

export function createOptions(data: any[]) {
  if (data[0].name) {
    return data.map((item: any) => createOption(item.name, String(item.id)));
  }
  return data.map((item: any) =>
    createOption(String(item.value), String(item.id))
  );
}
