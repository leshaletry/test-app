const { Sequelize } = require("sequelize");
import "dotenv/config";

var sequelize = new Sequelize(
  `postgres://postgres:${process.env.DB_PASSWORD}@localhost:5432/quiz`
);

export default sequelize;
