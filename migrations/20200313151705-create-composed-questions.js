'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('composedquestions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      question_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "questions",
          key: "id"
         }
      },
      level_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "levels",
          key: "id"
         }
      },
      topic_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "topics",
          key: "id"
         }
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ComposeQuestions');
  }
};