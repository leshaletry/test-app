"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("correctanswers", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      composed_question_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "composedquestions",
          key: "id"
        }
      },
      answer: {
        type: Sequelize.STRING
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("CorrectAnswers");
  }
};
